# **Projeto Exemplo Identificador**

| Data | Autor | Comentários | Versão |
| --- | --- | --- | --- |
| 21/05/2019 | Nome de usuário | Versao inicial | 1.0.0.RELEASE |  
| 22/05/2019 | Nome de usuário | Versao inicial | 1.0.1.RELEASE |  

## Objetivo ##

  Uma breve descrição resumida sobre o projeto.

----
## Diagramas ##
| Arquitetura |
| --- |
| [Exemplos Diagramas de classe 1](http://xtpo.com.br/abcd) |
| [Exemplos Diagramas de classe 2](http://xtpo.com.br/abcd) |

----
## Dependencias ##
* [library-core-autenticacao](http://xtpo.com.br/lib/_git/abcde)
* [library-dao-exemplo](http://xtpo.com.br/lib/_git/abcd)

----
## PBs ##
 * [0001](http://xtpo.com.br/work/_workitems?id=0001&_a=edit) - Resumo do Product Backlog 0001
 * [0002](http://xtpo.com.br/work/_workitems?id=0002&_a=edit) - Resumo do Product Backlog 0002
 
----
## Contratos - Input/Output/DTOs ##
### Core
| DTO |
| --- |
| [TransacaoDto](http://xtpo.com.br/lib/_git/abcd) |
| [AcessoDto](http://xtpo.com.br/lib/_git/abcd) |
| [AutenticacaoCore](http://xtpo.com.br/lib/_git/abcd) |
| [MensageriaCore](http://xtpo.com.br/lib/_git/abcd) |


### Inbounds
| Tecnologia | Nome da Recurso | Código do Evento | DTO |
| --- | --- | --- | --- |
| Rest | IdentificadorRestController | - | [IdentificadorDto](http://xtpo.com.br) |
| RabbitMQ | IdentificadorListener | - | [MensageriaDto](http://xtpo.com.br) |


### Outbounds
| Tecnologia| Nome da Recurso | Código do Evento | DTO |
| --- | --- | --- | --- |
| MongoDB | IdentificadorMongoDBServiceHandler | - | [IdentificadorDto](http://xtpo.com.br) |
| Redis | IdentificadorRedisService | - | [IdentificadorDto](http://xtpo.com.br) |
| RabbitMQ | IdentificadorRabbitMQerviceHandler | - | [IdentificadorDto](http://xtpo.com.br) |


----
## How To Build ##
```
mvn clean install
```

## How To Execute ##
 1. Copie o jar da aplicação para a pasta referente ao projeto em seu [docker-compose](http://xtpo.com.br/) local.
 2. Inicie os containers no [Docker](http://xtpo.com.br/wiki/Docker) conforme descrito no docker-compose.

----
## Configurações ##
Para este projeto não existem configurações especificas apenas as [configurações básicas](http://xtpo.com.br/wiki/Configura-o-da-M-quina-do-Desenvolvedor).

----
